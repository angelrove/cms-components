<?php
namespace angelrove\CmsComponents\Console;

use Illuminate\Support\ServiceProvider;

class ConsoleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadViewsFrom(__DIR__ . '/resources', 'datatables');
        $this->publishAssets();
        // $this->registerCommands();
    }

    /**
     * Publish assets.
     */
    protected function publishAssets()
    {
        $packageName = 'angelrove-console';

        // Conf ---
        $this->publishes([
            // __DIR__ . '/config/config.php' => config_path($packageName.'.php'),
        ], 'config');

        // Views ---
        $this->publishes([
            // __DIR__ . '/resources/views' => base_path('/resources/views/vendor/'.$packageName),
        ], 'views');

        // Assets ---
        $this->publishes([
            __DIR__ . '/assets/lib.js' => public_path('packages/'.$packageName.'/lib.js'),
        ], 'public');
    }

    /**
     * Register commands.
     */
    protected function registerCommands()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}
