<?php
/**
 * Leer: README_CONFIG.md
 */

namespace angelrove\CmsComponents\Console;

use angelrove\CssJsLoad\CssJsLoad;

class Console
{
    static private $flushBuffer;
    static private $activeBuffer;
    static private $key_session;

    public static function _init(string $console_id='console-sess', bool $activeBuffer=true): void
    {
        // key_session
        self::$key_session = $console_id;

        // Para cuando no es por ajax (false)
        self::$activeBuffer = $activeBuffer;

        self::cleanFlush();

        if (self::$activeBuffer) {
            ob_implicit_flush(true);
            self::$flushBuffer = str_repeat(' ', 1024*64);
        }

        // css / js ------
        CssJsLoad::set(__DIR__ . '/assets/lib.js');
    }

    public static function echo(string $string, string $color=''): void
    {
        // Color --------
        switch ($color) {
            case 'ok':
                $color = 'greenyellow';
                break;

            case 'fail':
                $color = 'darkorange';
                break;
        }
        if ($color) {
            $string = '<span style="color:'.$color.'">'.$string.'</span>';
        }

        // Session -------
        if (isset($_SESSION[self::$key_session])) {
            $_SESSION[self::$key_session] .= $string;
        }

        // Flush ---------
        if (self::$activeBuffer) {
            echo $string;
            echo self::$flushBuffer; // buffer
        }
        // No flush ------
        else {
        }
    }

    public static function endFlush(): void
    {
        $text = ob_get_clean();
        echo $text;

        $_SESSION[self::$key_session] .= $text;
    }

    public static function cleanFlush(): void
    {
        $_SESSION[self::$key_session] = '';
    }
}
