<?php
/**
 * Leer: README_CONFIG.md
 */

namespace angelrove\CmsComponents\Console;

use angelrove\CssJsLoad\CssJsLoad;

class ConsoleView
{
    private static $key_session;
    private static $framework;

    private static $iconsType = 'font-awesome';
    private static $listIcons = [
        'font-awesome' => [
            'expand' => 'fas fa-expand',
            'compress' => 'fas fa-compress',
        ],
        'line-awesome' => [
            'expand' => 'las la-expand-arrows-alt',
            'compress' => 'las la-compress',
        ]
    ];

    public static function get(string $console_id='console-sess', int $height=305, bool $getFlush=true): void
    {
        self::$key_session = $console_id;
        $icons = self::$listIcons[self::$iconsType];

        // Conf framework ---
        CssJsLoad::set(__DIR__ . '/assets/lib.js');

        // $secc = request()->segment(1);
        // $this->url_ajax = route($this->key_session);

        // View ---
        include_once __DIR__  . '/tmpl_console.php';
    }

    public static function getFlush(): mixed
    {
        return ($_SESSION[self::$key_session])?? '';
    }

    public static function confIcons($iconsType): void
    {
        self::$iconsType = $iconsType;
    }
}
