/**
 * Console
 *
 * reference: http://stratosprovatopoulos.com/web-development/php/ajax-progress-php-script-without-polling/
 *
 */

var console_id = '#console_out';

$(document).ready(function() {
    $("#Console #onResize").click(function(event) {
        event.preventDefault();
        $(console_id).css("height", "550px");
    });

    $("#Console #onResizeSmall").click(function(event) {
        event.preventDefault();
        $(console_id).css("height", "250px");
    });
});

//--------------------------------------------------------
function console_launchProcess(btn_process, ajax_url, method='GET', params='')
{
    /*
     * IMPORTANT!!
     * Laravel csrf token
     * more info: https://laravel.com/docs/7.x/blade#csrf-field
     */
    let csrf_token = document.querySelector("input[name='_token']");
    if (csrf_token) {
        params = '_token=' + csrf_token.value + '&' +params ;
    }

    // Console clear
    $(console_id).text('');

    try {
        //-------
        $(btn_process).button('loading');

        //-------
        var xhr = new XMLHttpRequest();
        xhr.previous_text = '';

        //----------------
        xhr.onerror = function () {
            alert("[XHR] Fatal Error.");
        };
        //----------------
        xhr.onreadystatechange = function () {
            try {
                //-----------------
                var new_response = xhr.responseText.substring(xhr.previous_text.length);
                xhr.previous_text = xhr.responseText;

                // Out
                $(console_id).append(new_response);

                // autoscrolling to the bottom
                $(console_id).scrollTop($(console_id)[0].scrollHeight);

                //-----------------
                if (xhr.readyState == 4) {
                    $(btn_process).button('reset');

                    // Reload page ---
                    // var r = confirm("Fin del proceso! \n¿Quieres recargar la página para ver los cambios?");
                    // if (r == true) {
                    //     location.reload();
                    // }
                }
            }
            catch (e) {
                alert("[XHR STATECHANGE] Exception: " + e);
            }
        };
        //-----------------

        // Calling ajax ---
        xhr.open(method, ajax_url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.send(params);
    }
    catch (e) {
        $(btn_process).button('reset');
        alert("[XHR REQUEST] Exception: " + e);
    }

}
//--------------------------------------------------------

