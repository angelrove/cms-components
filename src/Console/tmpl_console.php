<style>
#Console {
    position: relative;
}
#Console>#btOptions {
    position: absolute;
    top:6px; right: 25px;
}
#console_out {
    height: <?=$height?>px;
    background: #222; color:#fff; font-family: monospace;
    padding: 6px;
    width: 100%;
    border-width:0;
    text-align: left;
    overflow: scroll;
}
</style>

<div class="clearfix"></div>

<div id="Console">
    <div id="btOptions" >
        <button id="onResize" class=""><i class="<?=$icons['expand']?>"></i></button>
        <button id="onResizeSmall" class=""><i class="<?=$icons['compress']?>"></i></button>
    </div>

    <div id="console_out">
        <?php
        if ($getFlush) {
            echo self::getFlush();
        }
        ?>
    </div>
</div>
