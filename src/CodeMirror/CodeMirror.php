<?php
/**
 * @author José A. Romero Vegas <jangel.romero@gmail.com>
 * Use CodeMirror: https://codemirror.net/
 */

namespace angelrove\CmsComponents\CodeMirror;

use angelrove\CssJsLoad\CssJsLoad;
use angelrove\CssJsLoad\Vendor;

class CodeMirror
{
    private $name           = '';
    private $theme          = '';
    private $func_on_change = false;

    //---------------------------------------------------------------------
    public function __construct(string $name, string $type = '', string $theme = 'ambiance')
    {
        $this->name  = $name;
        $this->theme = $theme;

        // Codemirror lib ---
        include_once '_vendor.php';
        Vendor::usef('codemirror');

        //------------------
        CssJsLoad::set(__DIR__ . '/assets/styles.css');
        CssJsLoad::set(__DIR__ . '/assets/libs.js');
    }
    //---------------------------------------------------------------------
    public function set_function_on_change(): void
    {
        $this->func_on_change = true;
    }
    //---------------------------------------------------------------------
    public function show(string $value): string
    {
        if ($this->func_on_change) {
            $str_on_change = "WInputCode_change('$this->name');";
        }

        CssJsLoad::set_script("
var theme = '$this->theme';

$(document).ready(function() {

  editores['$this->name'] = CodeMirror.fromTextArea($('.codemirror-textarea_$this->name')[0], {
    selectionPointer: true,
    lineNumbers: true,
    styleActiveLine: true,
    matchBrackets: true,
    autorefresh:true,
    extraKeys: {'Ctrl-Space': 'autocomplete'},
  });

  editores['$this->name'].setOption('theme', theme);

  // on change --------------
  editores['$this->name'].on('change', function(e) {
    $str_on_change
  });

});
", 'WInputCode_' . $this->name);

        // Out ---
        $params = array(
            'name'  => $this->name,
            'value' => $value,
        );

        // return FileContent::include_return(__DIR__ . '/tmpl_main.inc', $params);
        return $this->includeReturn(__DIR__ . '/tmpl_main.inc', $params);
    }
    //---------------------------------------------------------------------
    public function includeReturn(string $file, array $params = array()): string
    {
        try {
            ob_start();

            if ((include ($file)) === false) {
                throw new \Exception("File not found [$file]");
            }
        } catch (\Exception $e) {
            ob_get_clean();
            throw $e;
        }

        return ob_get_clean();
    }
    //------------------------------------------------------------------
}
